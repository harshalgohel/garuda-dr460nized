[Appearance]
ColorScheme=Sweetified
Font=FantasqueSansMono Nerd Font Mono,12,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[Cursor Options]
CursorShape=1
CustomCursorColor=255,0,0
UseCustomCursorColor=true

[General]
Command=/usr/bin/fish
Name=Garuda
Parent=FALLBACK/
TerminalColumns=110

[Interaction Options]
AutoCopySelectedText=true
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=linux

[Scrolling]
HistoryMode=1

[Terminal Features]
BlinkingCursorEnabled=true
