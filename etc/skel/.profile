export AMD_VULKAN_ICD=RADV
export BROWSER=librewolf
export EDITOR=/usr/bin/micro
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
export MAIL=thunderbird
export TERM=konsole
export VISUAL=kate
